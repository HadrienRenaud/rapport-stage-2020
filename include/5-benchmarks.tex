
In this section\footnotemark, we provide a performance analysis of different solutions: the builtins \verb+Fut+ of Encore, the \DF\ futures that we introduced earlier, denoted as \verb+Flow+, and the control flow explicit futures that we presented in the previous section, denoted as \verb+Fut on Flow+.

\footnotetext{
    All the code for those benchmarks is available at: \href{https://hal.archives-ouvertes.fr/hal-02908763}{https://hal.archives-ouvertes.fr/hal-02908763}
}

We analyze several cases, using chains of futures of different length, or with different memory management patterns (allocating a new actor for each call or re-using a set of preallocated actors).

All the benchmarks results provided here are done on the same Dell XPS 13 9370, with an 8-core Intel Core i7-8550U and 16GiB of memory, running Ubuntu 20.04 and clang v10.0.
Encore at version \href{https://gitlab.inria.fr/lhenrio/encorewithdatafuts/-/tree/ea5736869d2ac34cfbeee2be4a2988c819a215af}{ea573686} is run using the release configuration and the -O3 flag.

\paragraph{Adaptation of the \texttt{Word Counter} example}
The Encore compiler is shipped with tests, one is a word counter, from which we adapted this example.
The \verb+Word Counter+ test dispatches hash table insertions to 32 actors.
We re-implemented the standard library module \verb+Big.HashTable.Supervisorh+ with \verb+Flow+, with \verb+Fut on flow+ or with sequential execution (refered as \verb+fun+).
There is no need here to use any \verb+forward+, because there is no chain of futures.
The original example used an optimization that removed the use of futures: send one-way messages when the caller does not need the result.
As this does not use futures, we disabled it in the \verb+Fut+ example, but still displayed its performance as \verb+OneWay+, which allows us to evaluate the cost of using futures.
This example can not be beneficial for \Flow{} s, because at runtime, \Flow\ only optimizes the chain resolution and this example does not use chains of futures.

\begin{table}[!htb]
    \centering
    \begin{tabular}{lcll}
        \toprule
        Future used        & Average running time & \multicolumn{2}{l}{Std deviation} \\
        \midrule
        \verb+OneWay+      & 26.3 ms & 0.5 ms & 2.0\% \\
        \verb+Fut+         & 35.4 ms & 0.6 ms & 1.7\% \\
        \verb+Flow+        & 35.4 ms & 0.6 ms & 1.7\% \\
        \verb+Fut on Flow+ & 35.8 ms & 0.7 ms & 1.9\% \\
        \verb+fun+         & 46.1 ms & 1.5 ms & 3.3\% \\
        \bottomrule
    \end{tabular}
    \caption{Running time of \texttt{WordCounter} (100 runs)}\label{tab:wordcounter}
\end{table}

Table~\ref{tab:wordcounter} shows the performance of the WordCounter example.
Obviously, the OneWay is far faster than the others, but from it, we can evaluate that using futures takes approximately 10ms.
At the other end, the \verb+fun+ is far slower than the others, which shows that parallelism with futures can speed up performance.
The little speedup between the sequential and parallel execution is due to the high number of communications compared to the small number of computations needed in this example.
All the implementations of futures perform similarly, and the simulation of \CF\ futures over \DF\ futures by the \verb+Fut on Flow+ example only has a marginal cost.

\paragraph{}
In the following, we focus on use cases for which the data-flow and control-flow futures entail different synchronization patterns.
The next examples focus on synchronization primitives and evaluate the cost of asynchronous communications and future access.
In particular, they do not exploit parallelism, nor oneway messages.

\paragraph{Tests with actors creation}
We implement the Ackermann function recursively, with a terminal call to build \verb+Flow+s chains of various lengths.
This example builds a new actor for each recursive call.

\begin{table}[hptb]
    \centering
    \begin{tabular}{lcll}
        \toprule
        Future used & Average running time & \multicolumn{2}{l}{Std deviation} \\ \midrule
        \verb+fun+                             & $\approx 1$ ms &        &       \\
        \verb+Fut+ with \verb+forward+         & 54.13 ms       & 3.1 ms & 5.8\% \\
        \verb+Fut on Flow+ with \verb+forward+ & 54.56 ms       & 1.8 ms & 3.4\% \\
        \verb+Flow+ with \verb+forward*+       & 57.37 ms       & 1.0 ms & 1.7\% \\
        \verb+Flow+                            & 58.75 ms       & 2.5 ms & 4.3\% \\
        \verb+Fut+                             & 64.38 ms       & 3.2 ms & 4.9\% \\
        \verb+Fut on Flow+                     & 65.37 ms       & 1.3 ms & 2.1\% \\
        \bottomrule
    \end{tabular}
    \caption{Average running time of the Ackermann function with creation of a new Actor at each call (arguments (3, 4) -- 100 iterations)}\label{tab:ack-creation}
\end{table}

Table~\ref{tab:ack-creation} shows the running times for this example.
As in the previous example, \verb+fun+ refers to the sequential evaluation of the example.
As the Ackermann example requires very few computations, it is far faster than the other executions.
The others are not very well separated and are at standard deviation one to another, which makes the example not very exploitable.
However, we can still conclude that for control-flow futures, using \verb+forward+ improves the performance.

To obtain more exploitable results, in the following example, we removed actor creation from the evaluated function, either by using the same actor or by preallocating actors for the task.

\paragraph{Recursive calls on a single actor}
As in the previous example, we implemented the Ackermann function with recursive calls, but this time on the same actor.
This benchmark removes the actor creation time from the results.

In this example, for all types of futures, using \verb+forward+ is not a substitute to a \verb+get+, but more a substitute for a non-blocking \verb+get+.
This non-blocking synchronization is needed because we only have one actor, otherwise, it would deadlock.
To do so, in Encore, we use an \verb+await+ on the future before calling the \verb+get+.

\begin{table}[hptb]
    \centering
    \begin{tabular}{lcll}
        \toprule
        Future used & Average running time & \multicolumn{2}{l}{Std deviation} \\ \midrule
        \verb+fun+                       & $\approx 1$ ms &        &       \\
        \verb+Fut+ with \verb+forward+   & 27.86 ms       & 1.2 ms & 4.3\% \\
        \verb+Fut on Flow+ with \verb+forward+
                                         & 28.10 ms       & 1.0 ms & 3.5\% \\
        \verb+Flow+ with \verb+forward*+ & 31.49 ms       & 0.7 ms & 2.2\% \\
        \verb+Flow+                      & 39.13 ms       & 1.0 ms & 2.4\% \\
        \verb+Fut+                       & 44.63 ms       & 0.7 ms & 1.7\% \\
        \verb+Fut on Flow+               & 46.71 ms       & 0.8 ms & 1.6\% \\
        \bottomrule
    \end{tabular}
    \caption{Average running time of the Ackermann function on the same actor  (arguments: 3, 4 -- 100 iterations)}\label{tab:ack-self}
\end{table}

Table~\ref{tab:ack-self} shows the performance evaluation for this example.
As expected, \verb+forward+ brings a great performance improvement, to all types of futures.
Outside the benefit of forward, \verb+Fut+ \verb+Flow+ and \verb+Fut on Flow+ with forward perform similarly.
However, without using \verb+forward+, \verb+Flow+ is far faster than control-flow futures.
This might be explained by the fact that the \verb+Flow+ version is tail-call recursive, and that the others need to wait for their futures resolution before returning.

More surprisingly, \verb+Fut on Flow+ with \verb+forward+ seems faster than \verb+Flow+ with \verb+forward*+, although \verb+Fut on Flow+ is a layer over \verb+Flow+.
One hypothesis for this is that the scheduler might prefer the future chains created by \verb+Fut on Flow+.

\paragraph{Recursive calls on many actors without actor creation}
We compute the sum of the numbers from 1 to $n$ by creating a linked list with those numbers and then asynchronously iterating on this list. The measured time does not take into account list creation.

\begin{table}[hptb]
    \centering
    \begin{tabular}{lcll}
        \toprule
        Future used & Average running time & \multicolumn{2}{l}{Std deviation} \\ \midrule
        \verb+Fut+ with \verb+forward+         & 16.03 ms & 1.0 ms & 6.4\% \\
        \verb+Fut on Flow+ with \verb+forward+ & 16.30 ms & 1.0 ms & 6.3\% \\
        \verb+Flow+ with \verb+forward*+       & 16.14 ms & 0.9 ms & 5.8\% \\
        \verb+Flow+                            & 26.02 ms & 1.3 ms & 5.1\% \\
        \verb+Fut+                             & 46.87 ms & 3.9 ms & 8.4\% \\
        \verb+Fut on Flow+                     & 47.64 ms & 3.8 ms & 8.0\% \\
        \bottomrule
    \end{tabular}
    \caption{Average running time of the loop on 10000 actors (100 iterations)}\label{tab:linkedlist}
\end{table}

Table~\ref{tab:linkedlist} shows the results of this example.
Note that this example shows the benefit of the \verb+forward+ construct in terms of performance, both for \verb+Flow+ and \verb+Fut+.
It also highlights the improvement of using \DF\ futures instead of \CF\@.

\paragraph{Discussion}
From these examples, we can conclude that on short chains, as the example of \texttt{WordCounter} shows, all futures implementations have roughly the same performance.
On longer chains without the use of \verb+forward+, \DF\ futures are more performant than \CF\@.
However, the use of \verb+forward+, while improving the performance of every solution, annihilate the difference between \verb+Flow+ and \verb+Fut+ on long chains.

In the \verb+Fut+ version, \verb+forward+ explicitly changes the semantics to relax synchronization.
In the \verb+Flow+ version, it becomes an optimization directive that doesn't change semantics.
Because \verb+forward*+ and \verb+return+ are interchangeable, \verb+return+ could be compiled to \verb+forward*+ and achieve automatically the best performance for \DF\@.
A clever compiler could have thus compiled the \verb+Flow+ version to \verb+Flow with forward*+ with the same semantics.
An implementation with \DF\ and this implicit optimization of \verb+return+ into \verb+forward*+ would ally both the performance improvements of \verb+forward+ and the improved type handling of \DF\@.

