In this section, we present how an implementation of $\nforwardstar$ can be typed.
As a mean of comparison, we will consider the $\nforward$ construct in the Encore programming language, which is similar to the \nforwardstar\ construction.

The formal typing rule \textsc{T-Forward} given in Figure~\ref{fig:fwd-rules} imposes that a function containing $\nforwardstar x$ must be typed as $s \rightarrow \fut t$ (with $ s $ abstracting away the function parameters).

If we look at the Encore language, a function containing $\nforward\ x$, with $ x :: \oldfut{t} $, is typed $s \rightarrow t$.
Calling the function asynchronously actually returns a \verb+Fut+.
This is in effect an alternate way to type $\nforward$.
To add the notion of \Flow\ from \DFFwd\ to the Encore language, one should consider a $\nforwardstar$ operator, akin to the existing $\nforwardstar$ but operating on \Flow\@.

There are now two possible ways to type $\nforwardstar$, the \DFFwd\ way called \emph{strict} mode in the rest of the paper, and the \emph{flexible} mode used for the implementation of $\nforwardstar$ in the Encore compiler. We will now discuss those two typing solutions.

\subsection{The strict way}
The first possibility is what we have introduced previously in \DFFwd. Coding a method with strict typing would be the following:
\begin{lstlisting}
  def work(arg: s): Flow[t]
    forward*(worker!compute(arg))
  end
\end{lstlisting}

Notice the presence of \Flow\ in the type of the result of the method. If the return type was \verb+int+, the user could trigger an implicit synchronization on a synchronous call to the function, for example by \verb+this.work(arg)+. Returning a future forces the user to trigger explicitly synchronizations, without being burdensome for the user as only one dataflow synchronization is needed.

Typing $\nforwardstar$ in Encore this way doesn't require more typing or semantic rules than what we have already established.

\subsection{The flexible way}
This way is used in the Encore compiler to type its own $\nforward$ construct. In Encore, $\nforward$ works on a \oldfut{t}. The return type of a function using Encore's $\nforward$ is then $t$, similarily to what is show here:
\begin{lstlisting}
  def work(arg: s): t
    forward worker!compute(arg)
  end
\end{lstlisting}
Notice the absence of \verb+Flow+ in the function return type.

To formalize this in \DFFwd, we consider a new typing rule and a new semantic rule.
\textsc{T-CeF-Forward} add the typing rule that allows flexible typing.
The runtime rule \textsc{CeF-Forward-Sync} enables a synchronous call of a method with forward.

{\small \begin{mathpar}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      T-CeF-FORWARD         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\inferrule[{\scriptsize T-CeF-ForwardStar}]
{ % 1)
  \Gamma \vdash \typed{e}{T'} \\
  % 2)
  \Gamma(\m) = \bar{T} \to T' \\
  }
%-------------------------------------%
{ \Gamma \vdash_\m {\nforwardstar} \ e }
%
\and
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          FORWARD           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\inferrule[\scriptsize CeF-ForwardStar-Sync]
{ \feval{v}{a+\ell} = f'
\\
y \text{ fresh variable}
}
{ a ~ f(\{\ell' \mid \nforwardstar\ v \semi s \} \# q \# \vect{q})
  \to
  a ~ f(\{\ell' \mid  y = \get f' \semi \nreturn\ y \semi s \} \# q \# \vect{q})
}
\end{mathpar} } % end of small
As in the original \DFFwd\ semantics, we still have the equivalence between $\nforward\ v$ and $\nreturn\ v$ in the asynchronous case.

\subsection{Benefits and drawbacks}\label{ssec:typing-forward-benefits-drawbacks}

\paragraph{Synchronous calls} The main argument against flexible typing is that it induces an implicit $\get{}$ synchronization when a method using forward is called synchronously, as shown in the semantic rule \textsc{Cef-ForwardStar-Sync}.
This drawback only applies to methods, as \nforward\ cannot be used inside functions.

\begin{figure}[htb]
\begin{lstlisting}
active class A
  def print_job_result(arg: s): t
    val result = this.work(arg)
    println("Result: {}", result)
    return result
  end
  def work(arg: s): t
    forward worker!compute(arg)
  end
end
\end{lstlisting}
    \caption{An example of a work-dispatcher with flexible typing}\label{fig:flexible-typing-dispatcher}
\end{figure}

Figure~\ref{fig:flexible-typing-dispatcher} shows an example of a synchronous call to a method using \verb+forward+ at line~3 where the function \verb+work+ is called.
Here, a call to \verb+print_job_result+ would implicitly block the actor on line~3.
This behavior is similar to the way implicit futures work: method \texttt{work}, written with futures in mind since it uses $\nforward$, can be called transparently like a method of a non-active class.

With the strict typing, the user will have to write an explicit \get\ to resolve the result of the synchronous call. This is expected from a language with explicit futures.

\paragraph{Backward compatibility}
The main advantage for using flexible typing is that code can seamlessly be translated from control-flow explicit futures $\oldfut{}$ to data-flow explicit futures $\fut{}$.
Let us consider the following example with control flow explicit futures:
\begin{figure}[htb]
\begin{lstlisting}
active class LinkedListNode
  val state: int
  val next: Maybe[LinkedListNode]
  -- ...
  def sum(acc: int = 0): int
    match this.next with
      case Just(next) => forward(next!sum(this.state + acc))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}
    \caption{A linked list of \texttt{int} with a method to compute its sum}\label{fig:flexible-linked-list}
\end{figure}

To port this code to \DFFwd, one can just change the asynchronous operators:  $\nforward$ into $\nforwardstar$ and \verb+!+ (asynchronous call returning a $\oldfut{}$) into \verb+!!+ (asynchronous call returning a $\fut{}$).
This operation is very easy and does not require any major changes in the code.
In particular, the type system of Encore with \DF\ includes the previously control flow explicit futures type system.

On the other hand, if we use strict typing, porting this code to Encore with \DF\ can be burdensome, as the return types of functions may change. This would require programmers to manually change the signature of all functions using \nforward\ and then track calls to these functions to change the code.

\subsection{Decision}

We choose to implement the flexible version into the Encore compiler, to provide strong backward compatibility between the \DF\ and the previous Encore compiler. That being said, it is recommended to type the methods using \verb+forward+ in a strict way, as to emphasize their asynchronous nature and prevent synchronous calls to asynchronous methods.

