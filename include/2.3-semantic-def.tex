To formally introduce \DF, Godot introduces a formal semantic.
We present a formal semantic that is adapted from Godot to our needs in the following sections.
This formal semantic is far smaller than Encore semantics~\cite{encore}, but also far simpler while containing all the elements needed for the work presented in this report.

The language is a lambda calculus derivative and only introduces a few constructs.
The main addition is futures and their manipulation keywords, as described in Section~\ref{ssec:intro-def}.

This language does not have actors, and we assume that this syntax can be expanded to a complete syntax with actors and asynchronous methods.
To design a complete syntax, one could insert asynchronous method calls based on what is done for \verb+async+ in this formal language, the difficulty is to assign only one thread to an actor.
This approach is based on what is done by Godot.

The language is based on the following expressions and values:
\begin{mathpar}
    e ::= v \bnfor e \ e \bnfor e \ [\tau] \bnfor \nreturn \ e \bnfor \texttt{async* } e \bnfor \getstar \ e \bnfor \texttt{then*} (e, e)
    \\
    v ::= c \bnfor x \bnfor f \bnfor \lambda X . e
\end{mathpar}
These expressions correspond to the keywords that we saw in Section~\ref{ssec:intro-def}, at the exception of \verb+await+ that is not needed here because there is no actor and no threads here.

\paragraph{Typing rules}
The types considered here are common basic types $K$, abstraction $\tau \rightarrow \tau$, types variables $X$ and the corresponding universal quantification $\forall X . \tau$, and \DF\ futures types $\Flow\ \tau$.
\[ \tau ::= K \bnfor \tau \rightarrow \tau \bnfor X \bnfor \forall X . \tau \bnfor \Flow\  \tau \]

Figure~\ref{fig:type-formation} defines the type system for \DF\@.
It is a System F type system with the \textsc{TF-Flow} rule to add $\Flow\ \tau$ types.
Note that the rule \textsc{TF-Flow} forbid the construction of a $\Flow \ \Flow \ \tau $.

\begin{figure}[hp]
    \begin{mathpar}
        \and
        \ntyperule{TF-Env}{}{\vdash \epsilon}
        \and
        \ntyperule{TF-EnvExpr}{x \notin \dom{\Gamma} \\ \Gamma \vdash \tau}{\vdash \Gamma, x : \tau}
        \and
        \ntyperule{TF-EnvVar}{X \notin \dom{\Gamma} \\ \vdash \Gamma}{\vdash \Gamma, X}
        \and
        \ntyperule{TF-K}{\vdash \Gamma}{\Gamma \vdash \mathcal{K}}
        \and
        \ntyperule{TF-Flow}{\Gamma \vdash \tau\\ \tau\neq\Flow{\tau'}}{\Gamma \vdash \Flow{\tau}}
        \and
        \ntyperule{TF-Arrow}{\Gamma \vdash \tau \\ \Gamma\vdash\tau'}{\Gamma \vdash \tau \to \tau'}
        \and
        \ntyperule{TF-X}{X \in \Gamma \\ \vdash \Gamma}{\Gamma \vdash X}
        \and
        \ntyperule{TF-Forall}{\Gamma, X \vdash \tau}{\Gamma \vdash \forall X.\tau}
    \end{mathpar}
    \caption{Type formation rules, with $ E ::= \epsilon \bnfor \Gamma, x : \tau \bnfor \Gamma, X$}\label{fig:type-formation}
\end{figure}

As shown in Figure~\ref{fig:async-fact}, \DF\ needs subtle handling of typing rules.
To do so, the flatten operator $\flatten{}$ reduces $\Flow\ \Flow\ \tau$ to $\Flow\ \tau$, where $\tau$ is a basic type:
{\small
\begin{mathpar}
    \flatten K = K
    \and
    \flatten X = X
    \and
    \flatten \forall X . \tau = \forall \flatten X . \flatten \tau
    \and
    \flatten (\tau \rightarrow \tau') = \flatten \tau \rightarrow \flatten \tau'
    \and
    \flatten \Flow\  (\Flow\  \tau) = \flatten \Flow\  \tau
    \and
    \flatten \Flow\  \tau = \Flow\  \flatten \tau \text{ if } \tau \neq \Flow\  \tau'
\end{mathpar}
} % end small

Typing rules are in figure \ref{fig:typing-expressions}.

Godot uses the $\diamond$ pseudo-type to prevent the use of \nreturn\ inside a lambda: the \textsc{T-Return} forbids typing $\nreturn\ \diamond$, and the return type of the function is passed from the abstraction (\textsc{T-Abstraction} or \textsc{T-TypeAbstraction}) to the return rule by $\rho$, which is either $\diamond$ or a type $\tau$.

To prevent typing expressions as $\Flow\ \Flow\ \tau$, the $\flatten{}$ operator is used when typing a \Flow\ (or a type abstraction).
To allow the use of a value in place of a \Flow, the type rule \textsc{T-ValFlow} allows a raise from $t$ to $\fut{t}$.

\begin{figure}[hp]
    {\scriptsize
    \begin{mathpar}
        \ntyperule{T-Constant}
            { \text{$c$ has type}\: \mathcal{K} \\ \Gamma \vdash \mathcal{K} }
            { \trule{c}{\mathcal{K}} }
        \and
        \ntyperule{ T-Variable }
            { x\: :\: \tau \in \Gamma \\ \vdash \Gamma }
            { \trule{x}{\tau} }
        \and
        \ntyperule{ T-Flow }
            { f : \Flow{\tau} \in \Gamma \\ \vdash \Gamma }
            { \trule{f}{\flatten{\Flow{\tau}}} }
        \and
        \ntyperule{ T-ValFlow }
            { \trule{e}{\tau}}
            {\trule{e}{\flatten{\Flow{\tau}}}}
        \and
        \ntyperule{T-Return}
            {\trulee{\tau}{e}{\tau} \\ \tau \neq \diamond \\ \Gamma \vdash \tau'}
            {\trulee{\tau}{\return{e}}{\tau'}}
        \and
        \ntyperule{T-Abstraction}
            {\Gamma,\: x\: :\: \tau\: \vdash_{\diamond}\: e :\: \tau'}
           {\trule{\lambda x.e}{\tau \rightarrow\: \tau'}}
        \and
        \ntyperule{T-Application}
            {\trule{e_{1}}{\tau\: \rightarrow\: \tau'} \\ \trule{e_{2}}{\tau}}
            {\trule{e_{1}\: e_{2}}{\tau'}}
        \and
        \ntyperule{T-TypeAbstraction}
            {\Gamma,\: X\: \vdash_{\diamond}\: e :\: \tau}
           {\trule{\lambda X.e}{\flatten{\forall X.\tau}}}
        \and
        \ntyperule{T-TypeApplication}
            {\Gamma, X \vdash_\rho e \: : \: \forall X.\tau'}
            {\trule{e\: [\tau]}{\flatten{\tau'[\tau/X]}}}
        \and
        \ntyperule{T-AsyncStar}
            {\trulee{\tau}{e}{\tau}}
            {\trule{\texttt{async* }e}{\flatten{\Flow{\tau}}}}
        \and
        \ntyperule{T-GetStar}
            {\trule{e}{\Flow{\tau}}}
           {\trule{\getstar e}{\tau}}
        \and
        \ntyperule{T-ThenStar}
            {\trule{e_1}{\Flow{\tau'}} \\ \trulee{\tau}{e_2}{\tau' \to \tau}}
            {\trule{\texttt{then*} (e_1, e_2)}{\flatten{\Flow{\tau}}}}
    \end{mathpar}
    } % end of scriptsize
    \caption{Typing of expressions}\label{fig:typing-expressions}
\end{figure}

\paragraph{Configurations}
To represent the parallelism of the execution, we use \emph{configurations}, which are multiset of:
\begin{itemize}
    \item tasks ($task_f \ e$) denote that the expression $e$ has to be evaluated and that it will fulfill the future $f$;
    \item chains ($chain_f \: g \: \lambda x.x$) specify that the closure has to be computed with the result of the future $g$ and that it will fulfill the future $f$;
    \item futures, either fulfilled with a value $v$ $\fflow{f}{v}$ or unfulfilled $\uflow{f}$.
\end{itemize}
Their syntax is formally defined by the following equation in which multiset union is denoted by a space:
\[
    \cconfig ::= \epsilon \bnfor \uflow{f}  \bnfor \fflow{f}{\!v} \bnfor \task{f}{e}  \bnfor chain_f \: f \: e  \bnfor  \cconfig{}\: \cconfig{}
\]
Configurations obey a set of rule (Figure~\ref{fig:well-formed-configs}), that are similar to typing rules, and that guaranty the correct typing of the futures and the logical soundness of the configuration in an environment $\Gamma$.
The rules (\textsc{t-config}) and (\textsc{T-Empty}) ensures the recursion from the base cases.
A configuration with an unfulfilled future is well-formed if the future is defined in the environment (\textsc{T-UFlow}), with a fulfilled future, the future needs also to be correctly typed (\textsc{T-FFlow}).
A configuration with a task is well-formed if the corresponding future has the correct type (\textsc{T-TaskFlow}).
The rule (\textsc{T-ChainFlow}) ensures that a chain correctly maps its types.

\begin{figure}[hp]
    {\scriptsize
    \begin{mathpar}
        \ntyperule{T-UFlow}
          {f \in \textit{dom} (\Gamma)}
          {\Gamma \vdash \uflow{f}}
        \and
        \ntyperule{T-TaskFlow}
          {f : \Flow\ \tau \in \Gamma
            \\
            \Gamma \vdash_{\tau} e : \tau}
          {\Gamma \vdash \task{f}{e}}
        \and
        \ntyperule{T-FFlow}
          {f : \Flow \ \tau  \in \Gamma
            \\
            \Gamma \vdash_{\diamond} v : \tau}
          {\Gamma \vdash \fflow{f}{v}}
        \and
        \ntyperule{T-ChainFlow}
          {f : \Flow \ \tau \in \Gamma
            \\
            g : \Flow \ \tau'  \in \Gamma
            \\
            \Gamma \vdash_{\tau} e : \tau'\to \tau}
          {\Gamma \vdash \chainsrc{f}{g}{e}}
        \and
        \nreduction{T-Empty}
          {\Gamma \vdash \epsilon}
        \and
        \ntyperule{T-Config}
            {\Gamma \vdash \textit{config}_{1} \\
                \Gamma \vdash \textit{config}_{2} \\
                \textit{config}_{1} \cap \textit{config}_{2} = \varnothing}
            {\Gamma \vdash \textit{config}_{1}\: \textit{config}_{2}}
    \end{mathpar}
    } % end of scriptsize
    \caption{Well formed configurations}
    \label{fig:well-formed-configs}
\end{figure}

\paragraph{Runtime semantics}
We divide the runtime semantic into two sets of rules: a local semantic that handle language structures in Figure~\ref{fig:runtime-semantics-def} and a distributed semantic that handle configurations in Figure~\ref{fig:config-runtime}.

The runtime semantics of configurations is in Figure~\ref{fig:config-runtime}.
Here we use the runtime test $\isflow{v}$ which indicates if the value $v$ is a \DF\ future or not.
When a task $\task{f}{g}$ finishes and produces a $\fflow{f}{v}$, the value $v$ is piped to the future $f$ that holds the result of this task (\textsc{R-FulfilFlow}).
Otherwise, if it is a static value $v$, this value fulfills the future $f$ (\textsc{R-FulfilFlowValue}).
The \textsc{R-ChainRunFlow} rule relates the run of the program: when a future $\fflow{f}{v}$ is fulfilled and chained via $(chain_g \: f \: e)$, then a new task is produced to compute the value of $e \: v$, which will eventually fulfill the future $f$.

\begin{figure}[hp]
    {\scriptsize
    \begin{mathpar}
        \ntyperule{R-FlowVal}
            {\neg\isflow{v}}
            {\task{f}{v}\ \uflow{f} \reduces{} \fflow{f}{v}}
        \and
        \ntyperule{R-FlowFut}
            {\isflow{g}}
            {\task{f}{g} \reduces{} (chain_f \: g \: \lambda x.x}
        \and
        \nreduction{R-ChainRunFlow}
            {(chain_g \: f \: e) \ \fflow{f}{v}
             \reduces{} \task{g}{(e\ v)}\ \fflow{f}{v}}
        \and
        \ntyperule{R-Config}
            {\cconfig{} \to \cconfig{''}}
            {\cconfig{}\: \cconfig' \to \cconfig{''}\: \cconfig{'}}
    \end{mathpar}
    } % end scriptsize
    \caption{Configuration runtime rules}
    \label{fig:config-runtime}
\end{figure}

With these configurations, we can now specify how the language behaves inside a given task.
We use a small step reduction with a $\bullet$ to indicate where the next step is in the evaluation context $E$:
\[
    E ::= \bullet \bnfor E \ e \bnfor \nreturn\ e \bnfor \getstar\ E \bnfor \texttt{then*}(E, e) \bnfor \texttt{then*}(v, E) \bnfor \texttt{unbox}\ E \bnfor E [\tau]
\]
The semantic rules are in Figure~\ref{fig:runtime-semantics-def}.
\textsc{R-$\beta$} and \textsc{R-TypeApplication} are System F $\beta$-reduction.
The other rules stipulate how to handle the different operators in \DF\@.
To reduce $\getstar\ f$, if $f$ is fulfilled, its value is evaluated (\textsc{R-GetStar}), otherwise, it loops on the following future (\textsc{R-GetVal}).
\texttt{async*} just spawn a new task with a corresponding new future (\textsc{R-AsyncStar}).
Chaining is done by spawning a new task if the original future is fulfilled (\textsc{R-ChainVal}), otherwise by recursively spawning a new chain for the first resolution of the future chain (\textsc{R-ChainFlow}).

\begin{figure}[hp]
    {\scriptsize
    \begin{mathpar}
        \nreduction{R-$\beta$}
            {\task{f}{E[\lambda x.e\: v]} \reduces \task{f}{E[e[v/x]]}}
        \and
        \nreduction{R-TypeApplication}
           {\task{f}{E[(\lambda X.e)\ [\tau]]} \to \task{f}{E[e[\tau/X]]}}
        \and
        \ntyperule{R-GetStar}
            {\isflow{g}}
            {\task{f}{E[\getstar g ]}\ \fflow{g}{v}
             \reduces{} \task{f}{E[v]}\ \fflow{g}{v}}
        \and
        \ntyperule{R-GetVal}
            {\neg\isflow{v}}
            {\task{f}{E[\getstar v ]} \reduces{} \task{f}{E[v]}}
        \and
        \ntyperule{R-AsyncStar}
            {\textit{fresh}\ f}
            {\task{g}{E[\texttt{async* } e]} \reduces{} \uflow{f}\ \task{f}{e}\ \task{g}{E[f]}}
        \and
        \nreduction{R-Return}
             {\task{f}{E[\return{v}]} \rightarrow \task{f}{v}}
        \and
        \ntyperule{R-ChainVal}
            {\neg\isflow{v} \quad \textit{fresh}\ g}
            {\task{f}{E[\texttt{then*} (v, \ \lambda x.e)]} \reduces{} \uflow{g}\ \task{g}{(\lambda x.e)\ v}\ \task{f}{E[g]}}
        \and
        \ntyperule{R-ChainFlow}
            {\isflow{h} \quad \textit{fresh}\ g}
            {\task{f}{E[\texttt{then*} (h, \ \lambda x.e)]} \reduces{} \uflow{g} \
             \chainsrc{g}{h}{\lambda x.e}\ \task{f}{E[g]}}
    \end{mathpar}
    } % end scriptsize
    \caption{Runtime semantics for \DF}\label{fig:runtime-semantics-def}
\end{figure}

With this semantics, Godot concludes by two theorems that show the progress and preservations properties of this semantics.
    This can be surprising for a reader used to Encore, where deadlocks can happen, but as Godot does not build actors, it uses only a safe subset of Encore.

Encore has many more features than this syntax exposes, using a far more Object-Oriented language.
The main feature omitted by Godot is the actor definitions, which would have stopped the progress theorems because of their shared stack.

