In an explicit futures language, there are some difficulties due relative to chains of futures, ie to a list $f_1 \leftarrow f_2 \leftarrow \dots \leftarrow f_n$ of $n$ futures that resolve each other.
When the length $n$ of the chain is unknown at compile type, we call it a chain of dynamic length.
This dynamic length causes typing problems for example in recursive functions.

Godot~\cite{fernandezreyes:hal-02302214} lists two difficult points that lead to a new construction:
\begin{itemize}
    \item How can a function return either a future or a direct result? The difficulty is in the time system.
        In Encore, the simpler way would be to create an \verb+Either[t, Fut[t]]+ type and handle the two cases at the caller side.
        This is referred in Godot as \emph{Type Proliferation problem}
    \item How can a function work with a chain of dynamic length?
        Besides the typing problems, this causes runtime problem because resolving a long chain of futures can be hard, as a call to \verb+get+ on every future in the chain is needed to get the real value out of the chain.
        That can have a big impact on the scheduler as if correctly ordered, the actor that depends on every future in the chain will be scheduled after the resolution of every future in the chain, and in the worst case, it is rescheduled between every future fulfillment.
        This multiplication of futures is refed by Godot as the \emph{Future Proliferation Problem}.
\end{itemize}

To answer that, Henrio~\cite{henrio:hal-01758734} proposes a new construction called Data-flow Explicit Futures (abbreviated as \DF) by opposition to the Control-flow Explicit Futures (\CF) that are the native Encore futures.
A \DF\ holds a list of \CF\ futures that corresponds to its delegated calls.
It is accessed as a single element and typed as \verb+Flow[t]+, independantly of the number of futures in the list.
This allows the typing of a tail-recursive function.
\DF\ type system allows the lift of a value of type \verb+t+ to a type \verb+Flow[t]+, which solves the Type Proliferation Problem.
A \DF\ synchronization corresponds to the synchronization of every \CF\ future in is holding list.
As the user does not interact with the list of futures, backstage optimizations can be made on this chain without breaking the semantic of the program, thus solving the Future Proliferation problem.

Figure~\ref{fig:async-fact} shows an example of such an asynchronous tail-recursive function.
Its return type is a \verb+Flow[int]+, and the typechecker raises the \verb+int+ as an already-resolved future.

\begin{figure}[htbp]
\begin{lstlisting}
active class A
    def fact(n: int, acc: int = 1): Flow[int]
        if n == 0 then
            return acc
        else
            return this!!fact(n * acc)
        end
    end
end
\end{lstlisting}
\caption{A simple tail-recursive asynchronous factorial}\label{fig:async-fact}
\end{figure}

This construction does not change the chain of scheduled futures. It only provides a convenient way to interact with it as a whole and brings flexibility in the typechecker by allowing a raw data type to be passed as an (already fulfilled, and not scheduled) future at line 4. At line 6, we call asynchronously the same method on the same object.

Our implementation of \DF\ in Encore provides equivalent primitives for the differents primitives handling futures in Encore:
\begin{itemize}[noitemsep]
    \item the call operator \verb+!+ becomes \verb+!!+, and now produces a \DF\ future instead of a control flow one.
    \item \verb+get+ becomes \verb+get*+, and now blocks and waits for every future in the chain
    \item \verb+await+ becomes \verb+await*+, and now waits for every future in the chain.
    \item \verb+async+ becomes \verb+async*+, and now returns a \DF\ future
    \item \verb+then+ becomes \verb+then*+, and now handle \DF\ futures.
    \item \verb+forward+ becomes \verb+forward*+, as described in Section~\ref{ssec:intro-forward}
\end{itemize}
We choose to name the new stared operators differently from their control flow counterparts not to confuse the two.
This is not needed in practice, and another implementation could choose the same names.

Figure~\ref{fig:example-with-proactive}.b shows an use of these new operators.
Notice that thanks to the automatic raise of values to flows, the function \verb+foo_fut+ at line 13 of Figure~\ref{fig:example-with-proactive}.a is no longer needed.
Moreover, only one synchronization is needed to \verb+get+ the value from a chain of 2 elements (Figure~\ref{fig:example-with-proactive}.a on line 21 and Figure~\ref{fig:example-with-proactive}.b on line 16).

