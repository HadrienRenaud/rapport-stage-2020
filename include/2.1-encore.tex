\paragraph{Actors}
All the implementation presented here are written for the Encore compiler~\cite{encore}.
Encore uses an object-oriented paradigm with capabilities, including the \verb+active+ capability, which makes the Encore language an \emph{actor-based language}: an \verb+active+ object is an actor.
Communication between actors are ordered with causal ordering and are abstracted from the programmer, who only makes asynchronous calls on methods of \verb+active+ objects.
The results of those functions calls are stored in Futures.

\paragraph{Futures}
Futures are simple empty boxes that represent future results and that will be fulfilled later.
When an actor \verb+a+ calls a method \verb+foo+ on actor \verb+b+, a future is created in \verb+a+'s namespace.
When \verb+b+'s \verb+foo+ method returns, the result will be placed in the future.
The future is now fulfilled and \verb+a+ will be able to use it.
A future is a read-only object, unlike promises that can be programmatically fulfilled where their handler is known.

\paragraph{Explicit or implicit futures}
Encore futures are \emph{explicit}, which means that the user states when the actor's thread will await the resolution of the future.
In an \emph{implicit} futures language, such as ProActive or Smalltalk, the programmer does not have control over those synchronizations, which happens when the program needs the value of an unfulfilled future.
To do so, Encore has two keywords that trigger a synchronization: \verb+get+ which blocks the actor's thread until the future is fulfilled, and \verb+await+ which waits for the future fulfillment without blocking.

\begin{figure}[p]
    {\scriptsize
    \begin{minipage}[t]{.5\textwidth - 0.5em}
\begin{lstlisting}[captionpos=t, title={a. Encore with \CF}, label={lst:encore-explicit-futures}]
active class B
  def bar(t: int): int
    t * 2
  end

  def foo(x: int): Fut[int]
    val t = x + 1
    val beta = new B()
    beta!bar(t)
  end

  -- we need this function as foo cannot take both fut and int
  def foo_fut(x: Fut[int]): Fut[int]
    this.foo(get(x))
  end
end

active class Main
  def main(): unit
    val alpha = new B()
    val x: Fut[Fut[int]] = alpha!foo(1)
    val y: int = get(get(x)) + 1
    val z: Fut[Fut[int]] = alpha!foo_fut(get(x))
    println(get(get(z)))  -- 10
  end
end
\end{lstlisting}
    \end{minipage}
    \quad
    \begin{minipage}[t]{.5\textwidth - 0.5em}
\begin{lstlisting}[captionpos=t, title={b. Encore with \DF}, label={lst:encore-dataflow-explicit-futures}]
active class B
  def bar(t: int): int
    t * 2
  end

  def foo(x: Flow[int]): Flow[int]
    val t = get*(x) + 1
    val beta = new B()
    beta!!bar(t)
  end
end

active class Main
  def main(): unit
    val alpha = new B()
    val x: Flow[int] = alpha!!foo(1) -- this lifts 1 from int to Flow[int]
    var y: int = get*(x) + 1
    val z: Flow[int] = alpha!!foo(x)
    println(get*(z))  -- 10
  end
end
\end{lstlisting}
    \end{minipage}

\begin{lstlisting}[captionpos=t, title={c. ProActive (implicit futures)}, label={lst:proactive}, language=Java]
public class B {
    // ... define newB, main ...

    public WrappedInt bar(WrappedInt t) {
        return t.mult(2);
    }

    public WrappedInt foo(WrappedInt x) throws Exception {
        WrappedInt t = x.add(1);
        B beta = newB();
        return beta.bar(t);
    }

    public static void realMain() throws Exception {
        B alpha = newB();
        WrappedInt x = alpha.foo(new WrappedInt(1));
        WrappedInt y = x.add(1); // Forces synchronization
        WrappedInt z = alpha.foo(x);
        System.out.println(z); // Forces synchronization
    }
}
\end{lstlisting}
} % end of {\scriptsize

    \caption{A simple actor example: explicit, implicit, and DataFlow Explicit Futures}\label{fig:example-with-proactive}
\end{figure}

Figure~\ref{fig:example-with-proactive}.a shows an example of a simple Encore program, with two active classes, an actor creation at line 11, a asynchronous call with the operator \verb+!+ at line 12, which creates a future, and the resolution of this future at line 13 with the operator \verb+get+.
Calling \verb+get+ blocks the thread of the actor until the future is resolved.
Figure~\ref{fig:example-with-proactive}.c displays an extract of a ProActive code, with implicit futures.
As we can see at line 16, the programmer does not type its futures as \verb+Fut[t]+ but directly as \verb+t+.
The synchronization of lines 17 and 19 which are triggered by the call of the method on those objects are implicit: no keyword nor method to trigger it.
This language feature hides the asynchronicity of the code from the programmer and eases its typing but it becomes harder to understand what is happening.
The function \verb+foo_fut+ at line 12 of Figure~\ref{fig:example-with-proactive}.a is needed as the function \verb+foo+ can not take both type \verb+int+ or type \verb+Fut[int]+, which is not the case with implicit futures, where being a future is not reflected on the type system.

Besides \verb+get+ and the call operator \verb+!+ that are shown in this example, Encore provides the programmer with more primitives: \verb+await+, \verb+then+ and \verb+async+.
\verb+await f+ waits in a non-blocking manner for the resolution of the future \verb+f+.
\verb+then(f, e)+ execute the closure \verb+e+ when the future \verb+f+ is completed and returns a future containing the result of \verb+e+. This operation is called \emph{chaining}.
\verb+async e+ spawn a task which aims at evaluating \verb+e+ and returns a future that will be fulfilled by the value of \verb+e+.
Furthermore, following~\cite{fernandezreyes:hal-02302214}, the Encore language accepts the \verb+forward+ primitive as described in section \ref{ssec:intro-forward}.


\paragraph{Encore compiler and runtime}
Encore is a high-level language that compiles to C.
Its compiler is written in Haskell and its runtime uses the backend of Pony, for example for actor creation and message sending~\cite{encore}.
Pony implementation of actors uses green-threads so the threading capabilities of the Encore runtime are not limited by the OS or the number of cores on a machine~\cite{pony}.
Encore is a garbage-collected language, but due to its specialization, garbage collection runs only between two message treatment.

