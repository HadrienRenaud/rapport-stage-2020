
To collapse a chain of futures, Fernandez-Reyes and al~\cite{fernandez2018forward} introduce the \verb+forward+ construct.
This keyword can be used to replace a \verb+return+ in an asynchronous method with an asynchronous tail call.
Instead of creating a new future for the tail call, this passes the current future to the called method.
This process is called \emph{delegation} of the fulfillment of a future.
A tail-recursive function can then run in constant space because only the first future is used for all the functions calls.

Figure~\ref{fig:ex-forward-cf} shows an example of a tail-recursive asynchronous factorial using \verb+forward+ and control-flow futures.
We can see that the method is typed as if it returned an \verb+int+, which is permitted by the \verb+forward+ on line 6.
The use of \verb+forward+ can solve the Type Proliferation Problem by handling the asynchronous part of the conditional typed as a synchronous operation: here line~6 is typed as an \verb+int+ which makes the two branches equally typed.

\begin{figure}[htb]
\begin{lstlisting}
active class A
    def fact(n: int, acc: int = 1): int
        if n == 0 then
            return acc
        else
            forward (this ! fact(n - 1, acc * n))
        end
    end
end
\end{lstlisting}
    \caption{An asynchronous factorial in Encore with \texttt{forward} and control flow futures}\label{fig:ex-forward-cf}
\end{figure}

Although \verb+forward+ was firstly designed for control flow futures (see Godot, Forward to a promising Future), we adapted it to dataflow explicit futures.
In this section, we provide semantic rules for \verb+forward*+, which is the equivalent of \verb+forward+ for \DF\@, designated in the following as \DFFwd\@.

Figure~\ref{fig:fwd-rules} shows the type system and the semantic rules of forward.
The type system of \verb+forward*+ (\textsc{T-ForwardStar}) is very simple, as it is the same as \verb+return+ while ensuring that the return type is a \DF\@:
The runtime semantics of \verb+forward*+ (\textsc{R-ForwardStar}) relies on the \textit{chain} operation to bind the result of the next future to the one relative to the current task.
In a complete syntax, we would have to specify the behavior of \verb+forward*+ in a synchronous call, but here there are no synchronous calls.
A possible optimization can be done if the future passed to \verb+forward*+ is already fulfilled: we can eagerly fulfill the result of the current task (\textsc{R-Forward-Resolved}).

\begin{figure}[htb]
    \begin{mathpar}
        \ntyperule{T-ForwardStar}
            {\Gamma \vdash \tau' \\ \tau = \flatten \Flow\ \tau'' \\ \trulee{\tau}{e}{\tau}}
            {\trulee{\tau}{\nforwardstar\ e}{\tau'} }
        \and
        \nreduction{R-ForwardStar}
            {\task{f}{E[\nforwardstar\ h]} \rightarrow{} \chainsrc{f}{h}{\lambda x.x}}
        \and
        \nreduction{R-Forward-Resolved}
            {\task{f}{E[\nforwardstar\ h]} \ \fflow{h}{v} \rightarrow{} \fflow{f}{v} \ \fflow{h}{v} }
    \end{mathpar}
    \caption{Typing rules and semantics of \texttt{forward*}}\label{fig:fwd-rules}
\end{figure}

The operator \verb+forward+, designed by Fernandez-Reyes and al for control flow futures, has the same runtime semantics as our \verb+forward*+ operator.
However, its typing rules collapse the return type of the function to the value:
\[
    \ntyperule{T-Forward}
        {\Gamma \vdash \tau' \\ \trulee{\tau}{e}{\texttt{Fut}\, \tau}}
        {\trulee{\tau}{\nforward\ e}{\tau'}}
\]
This makes \verb+forward(x)+ equivalent in type to \verb+return get(x)+ without needing any synchronization.
On the other hand, \verb+forward*(x)+ is more equivalent in type to \verb+return x+, and previous studies~\cite{forward2020} show this last equivalence is also a complete semantics equivalence.
More precisely, we~\cite{forward2020} showed that rewriting a program with \verb+forward*(x)+ to \verb+return x+ preserves its semantics.

