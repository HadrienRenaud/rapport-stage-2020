\subsection{Motivation}
A goal of our study is to provide a better way of interacting with futures than the current one.
As control-flow futures are far more used than dataflow futures, we have to ensure backward-compatibility between them.
To do so, we build a library implementation of \CF\ futures on top of \DF, based on the theory introduced by Godot.
While Godot provides a semantic construction for implementing \CF\ futures over a \DF\ backend, it does not provide an implementation.

It is interesting to note that Godot provided an implementation for the reverse process, ie \DF\ over \CF, in scala (this is the artifact associated to~\cite{fernandezreyes:hal-02302214}).
However, this implementation does not support parametric types, due to a difficulty in introspecting parametric types at compile time.
Today, we do not have a solution for fixing this because the only known way to deal with \DF\ and parametric type relies on the collapse operation that does not exist in Scala's type system.
This shows that it is easier to build a \DF\ backend and then implement a \CF\ module for compatibility over it, than just implementing \DF\ as a library on top of \CF\@.

\subsection{Definition}
In this section, we want to build \CF\ futures, on top of \DF, to assert its backwards compatibility with existing systems: a language implementing only $\fut{}$ as a primitive can rebuild $\oldfut{}$ as a library. Godot~\cite{fernandezreyes:hal-02302214} suggests a construction for such control-flow futures relying on \DF\@:
\[
\oldfut \tau ::= \Box\: \fut \tau
\]
$\Box$ is the ``box'' operator, that encapsulate its argument in a structure of a different type, whose only available operation is \texttt{unbox}, where $\unbox\ \Box x = x$.
Intuitively, the $\Box$ operator stops type flattening: $\flatten{\fut{\fut{T}}}$ reduces to $\flatten{\fut{T}}$, but $\flatten{\fut{\Box\fut{T}}}$ does not.
This makes the newly created $\oldfut\ $ behave similarly as a \CF\ future, as defined by Godot.

The corresponding operations follow:
\begin{mathpar}
    \get\ e::= \getstar{} (\unbox\ e)
    \and
    \nthen(e, f)::= \Box\, \nthen\texttt{*} (\unbox\ e, f)
    \and
    \nforward\ e::= \texttt{forward* } e
\end{mathpar}

\paragraph{Semantic}
Formally, we insert this box operation in the rules of Section~\ref{ssec:def-semantics}.
This means modifying the basic syntax for expressions and types:
\begin{mathpar}
    e' ::= e \bnfor \Box\ e' \bnfor \unbox\ e'
    \and
    v' ::= v \bnfor \Box\ v'
    \and
    \tau' ::= \tau \bnfor \Box\ \tau
    \and
    E' ::= \Box E \bnfor \unbox\ E
\end{mathpar}
with the flattening rule $\flatten \Box \tau = \Box \flatten \tau$.
The complete static semantic rules are very simple:
\begin{mathpar}
    \inferrule[{\scriptsize (Box)}] {
        \Gamma \vdash \tau
    } {
        \Gamma \vdash \Box \tau
    }
    \and
    \inferrule[{\scriptsize (T-Box)}] {
        \Gamma \vdash_\rho e : \tau
    } {
        \Gamma \vdash_\rho \Box\ e : \Box\ \tau
    }
    \and
    \inferrule[{\scriptsize (T-Unbox)}] {
        \Gamma \vdash_\rho e : \Box\ \tau
    } {
        \Gamma \vdash_\rho \unbox\ e : \tau
    }
\end{mathpar}
Finally, we just have to add a simple runtime rule to handle unboxing:
\begin{mathpar}
    \inferrule[{\scriptsize (R-Unbox)}] {} {
        \task{f}{E[\unbox\ (\Box\ v)]} \rightarrow \task{f}{E[v]}
    }
\end{mathpar}
With this operator $\Box$, we can directly type the \verb+get+, \verb+then+, \verb+async+ and \verb+forward+, and obtain the same type system as \CF\@.

\subsection{Implementation}
For our implementation we will work on the Encore programming language, in which the $\texttt{get*}$, $\texttt{then*}$ and $\texttt{async*}$ operations are implemented for \Flow\@.
We now want to get back to control flow futures.
We build a library that provides control-flow futures and their manipulation primitives without modifying the Encore compiler.
We thus show that the synchronization primitives of \DF\ futures are sufficient to provide futures with control-flow synchronization; additionally, the encoding of \CF\ futures can be done as a simple library.

With the previous definitions, we can define a type $\Flow\ \tau $ and some functions to interact with it.
In Encore, we define the class \verb+Future[t]+ and the \verb+get_+\footnote{Underscore appended to avoid name collision with the \texttt{get} reserved keyword} function as follows:
\begin{lstlisting}
read class Future[t]
    val content: Flow[t]
    def init(x: Flow[t]): unit
        this.content = x
    end
end

fun get_(y: Future[t]): t
    return get*(y.content)
end

-- define async_, forward_, await_, call_ ...
\end{lstlisting}

\paragraph{Library limitations}
Because we want to rely on a lightweight implementation that is only a library, some limitations arise.
For example, we cannot use direct calls, \verb+forward+ nor \verb+then+ syntax, as in Encore.
We provide helper functions that can be used as an interface to handle the new control-flow types.
For a coding experience that mimics what is currently done in Encore, syntactic suggar expansion has to be done.
For \verb+get+, \verb+then+, \verb+await+, \verb+async+, we provide functions that completely replace their native counterparts, their implementation is very similar to the previous example with \verb+get+.
However for the call operator \verb+!+ and for \verb+forward+, we cannot provide a complete replacement for the native keywords, without modifying the compiler.
We provide boxing or unboxing aliases that have to be used against the result or the argument of the \verb+Flow+ keywords.
For example, instead of writing \verb+forward(x)+, the programmer will have to use \verb+forward*(on_forward(x))+.

\begin{figure}[hbt]
\begin{lstlisting}
active class A
    def foo(x: int): int
        x * 2
    end

    def bar(x: int): int
        forward*(on_forward(on_call(this!!foo(x * 2))))
    end
end

active class Main
    def main() : unit
        val a = new A
        val four: Future[int] = on_call(a!!bar(1))
        val four = then_(four, logger)
        get_(four) -- 4
    where
        fun logger(x: int): int
            println (x)
            x
        end
    end
end
\end{lstlisting}
    \caption{Using Fut on Flow in an Encore program}\label{fig:fut-on-flow-example}
\end{figure}

The example in Figure~\ref{fig:fut-on-flow-example} shows a code using this library to perform \CF\ operations.
The complex line 7 replaces a simple \verb+forward(this!foo(...))+ in native Encore syntax.
On the other side, at lines 15 and 16, the primitives \verb+get+ and \verb+then+ are correctly replaced by \verb+get_+ and \verb+then_+ without increasing the complexity of the code.
Note that the return type of the method \verb+A.bar+ can be \verb+int+ thanks to the use of flexible typing introduced in Section~\ref{sec:typing-forward}.

Furthermore, these limitations are only due to our choice not to edit the compiler, so we think that fully integrating \verb+Fut on Flow+ with an already existing language should not cause any problem more serious than syntax conflicts.
Precisely, a practical syntax for these operations would only be syntactic suggar for the helper functions designed here.
A pre-processor or a C-like macro could expand this syntactic suggar to use the functions of this library, e.g.\ expand \verb+forward(this!foo(...))+ into line~7 of Figure~\ref{fig:fut-on-flow-example}.

\subsection{Conclusion}
With this implementation of Fut on Flow, we show that it is feasible to implement control-flow features over data-flow explicit futures.
We show that \DF\ is backward compatible with \CF\ and that this point is far easier than reimplementing control-flow futures from scratch.
Although our library is not user friendly, we believe that achieving a sympathetic syntax is only a matter of syntactic suggar, in this case, and thus can be implemented without much trouble in a compiler.

