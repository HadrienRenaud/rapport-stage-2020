
In some concurrent languages, the results of asynchronous methods are returned ahead of schedule in futures or promises~\cite{boer2017survey}.
Until their fulfillment, futures are just empty boxes; after their fulfillment, futures are read-only pointers to their value~\cite{BakerHH77}.
These objects are fulfilled concurrently and serve as a way to handle data across different concurrent contexts.
In actor-based languages, they abstract message passing which simplify greatly the handling of a message response.
Promises are futures but expose a handle to fulfill them, contrarily to futures that can only be fulfilled by the language runtime.

A common way of classifying futures is to separate \emph{explicit} from \emph{implicit} futures.
In a language with implicit futures, futures are hidden from the programmer and handled only by the language.
On the contrary, explicit futures are handled directly by the program with special keywords.
Where implicit futures let more freedom to the language to optimize the program asynchronous operations, explicit futures ensure that their usage is coherent with the program.
The main goal of the following designs is to find an intermediate between implicit and explicit futures with the benefits of both solutions.

\paragraph{Dataflow Explicit Futures}
When futures are exposed to the programmer, their interface reflects directly as the way they are stored in memory: the possible operations work on the box memory.
Future synchronization, ie blocking the current thread until the future is resolved and fetch its value, waits for the value to be put in the box, no matter if the value in the box is another future.
Another system has been conceived~\cite{henrio:hal-01758734}, where the future exposed to the program is closer to the data: a synchronization will wait until a real-world value has been computed and put to the futures.
We refer to the former solution as \emph{control-flow explicit futures} and to the latter as \emph{dataflow explicit futures}.
The new solution is to expose to the program an object which corresponds to an unknown at compile-time number of futures that are fulfilled one into another.
%% TODO rephrase here
The result of a synchronization thus cannot be a future and is plain data.
This reduces the burden of typing asynchronous methods, and for example, it allows a method to return either a future or data.

\paragraph{Future fulfillment and delagation}
On another hand, another tool for handling futures that fulfill futures has been designed~\cite{fernandez2018forward}.
Instead of resolving a future into another, this construct enables the code that fulfills the first future to delegate this fulfillment to the last caller, thus preventing the creation of the second future.
This avoids the creation of non needed futures, and thus removes the need for non wanted synchronizations.
This can leads to significant speed-ups.

\paragraph{Contributions}
Previous work has set the theoritical bases of dataflow explicit futures~\cite{henrio:hal-01758734}, and of delegations~\cite{fernandez2018forward} (Section~\ref{sec:previously}).
In a paper to be submitted~\cite{forward2020}, we provided an implementation of these tools in the programming language Encore.
This report aims to prove the usability of this implementation on the following points:
\begin{itemize}
    \item \emph{backward compatibility} of the dataflow explicit futures to the respect of control-flow explicit futures, ie that a library using control-flow explicit futures could work on a dataflow explicit futures language without much trouble (Section~\ref{sec:fut-on-flow});
    \item \emph{performance} of these new future construction in comparison with the control-flow explicit futures, by comparing the different future implementations on various benchmarks (Section~\ref{sec:benchmarks}).
    \item Additionally, we analyze how to properly mix dataflow explicit futures and future delegation (Section~\ref{sec:typing-forward}).
\end{itemize}

