The main goal of this repport is to study how \DF\ behaves in practice, and ensure that it can be implemented while respecting existing constructs.

\subsection{\DF\ advantages: from a developper perspective}
\label{ssec:df-advantages}
For a developper, using \DF\ instead of \CF\ can have several advantages.
\begin{itemize}
    \item First, the main advantage of \DF\ is that the length of the future chain can be unknown.
        This enables work on the future chain without handeling too much cases.
        By providing a single type non dependant of the length of the futures chain \DF\ enable recursive asynchronous functions.
    \item Raising values as already-fulfilled futures also removes unuseful cases such as what is presented in figure \ref{fig:example-with-proactive}, where the function \verb+foo_fut+ only exists because of ControlFlow futures are not raised.
        It also resolves the type proliferation problem (see section \ref{ssec:encore}).
    \item \DF\ also removes forced synchronization for typing reasons.
        For example, a \verb+get+ could be used to reduce the length of the future chain to comply to a return type, such as in Listing \ref{lst:fut} to type a recursive asynchronous function.
    \item While \verb+forward+ can solve part of those problem, we believe that it is complicated to use and bring certain typing problems, see section \ref{sec:typing-forward}.
    \item \suggestion{talk about builtin forward* ? or at least integration flow with forward}
\end{itemize}

The main question that needs answering is : how will \DF\ behave in practice ? Is it implementable ? Is it efficient ?


\subsection{Backward compatibility}
To be adopted, \DF\ needs to enable progressive adoption from the community.
To achieve that, one key factor is backward compatibility.

Here we need to enable control flow users with the means to go on using it, and in the same time shift to dataflow controls on futures.

One problem is that \DF\ needs introspection into the running types.
As we have seen in section \ref{include/2.3-semantic-def}, the runtime flag $\isflow$ is needed to correctly handle recursion when synchronizing or chaining futures.
As of today, we do not have a complete solution to implement this using only language constructs.
For example, Godot presented a librairy implementation of \DF, in Scala that did not work on parametric types.

As a consequence, we believe that \DF\ needs to be implemented at compiler level.

On the contrary, control flow futures do not need runtime type inspection, so we can implement it as a librairy, on top of dataflow explicit futures.
\TODO{find an argument why not both at compiler level}.
The first result of this report is that implementation (section \ref{sec:fut-on-flow}), that we will call \texttt{Fut on flow}.

\subsection{Comparaison}
As introduced in subsection \ref{ssec:intro-forward}, Godot introduces a \verb+forward+ construction that is used by the Encore compiler for \CF.
This construction can be combined with \DF\ to give the \verb+forward*+ construction, which is roughly equivalent to its \CF\ conterpart.

With the librairy implementation of the previous subsection, we now have 6 different constructions:
\begin{itemize}
  \item Native futures with \CF\ without using \verb+forward+
  \item Native futures with \CF\ with \verb+forward+
  \item \DF\ futures without \verb+forward*+
  \item \DF\ futures with \verb+forward*+
  \item \verb+Fut on Flow+ futures without \verb+forward+
  \item \verb+Fut on Flow+ futures with \verb+forward+
\end{itemize}
The goal of this study is to implement and compare these different methods, and provide cases where using one or the other is better.

\begin{figure}[p]
    {\small
\begin{lstlisting}[caption={Native \CF\ futures}, label={lst:fut}]
active class Node
  -- ...
  def sum(acc: int): int
    match this.next with
      case Just(next) => get(next!sum(this.state + acc))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}

\begin{lstlisting}[caption={Native \CF\ futures with \texttt{forward}}, label={lst:ffut}]
active class Node
  -- ...
  def sum(acc: int): int
    match this.next with
      case Just(next) => forward(next!sum(this.state + acc))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}

\begin{lstlisting}[caption={\DF\ futures}, label={lst:flow}]
active class Node
  -- ...
  def sum(acc: int): Flow[int]
    match this.next with
      case Just(next) => next!!sum(this.state + acc)
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}

\begin{lstlisting}[caption={\DF\ futures with \texttt{forward*}}, label={lst:fflow}]
active class Node
  -- ...
  def sum(acc: int): Flow[int]
    match this.next with
      case Just(next) => forward*(next!!sum(this.state + acc))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}

\begin{lstlisting}[caption={Fut on flow futures}, label={lst:futh}]
active class Node
  -- ...
  def sum(acc: int): int
    match this.next with
      case Just(next) => get_(call_(next!!sum(this.state + acc)))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}

\begin{lstlisting}[caption={Fut on flow futures with \texttt{forward}}, label={lst:ffuth}]
active class Node
  -- ...
  def sum(acc: int): int
    match this.next with
      case Just(next) => forward*(forward_(call_(next!!sum(this.state + acc)))
      case _          => this.state + acc
    end
  end
end
\end{lstlisting}
    } % end of small
    \caption{A comparison of different solutions}
    \label{fig:motivation-example}
\end{figure}

Figure \ref{fig:motivation-example} shows an example of a program using these techniques. We hide the construction, execution and initialization of the class, to emphasize on the differences on the different language constructions:
\begin{itemize}
    \item The return type of the function is a \verb+Flow[int]+ in \DF, and only \verb+int+ in \CF.
    \item Without forward, recursive function have to call \verb+get+
    \item \DF\ raises \verb+int+ to \verb+Flow[int]+ which suppress the need to call \verb+get+ on the returned future.
    \item We see that for the moment Fut on flow is only a librairy, and requires the usage of helper functions such as \verb+forward_+ or \verb+call_+.
\end{itemize}
In that case, the improvement promised by the subsection \ref{ssec:df-advantages} seems to correspond.

\subsection{Goals}
\label{ssec:goals}

While Godot \TODO{cite} defines a construction and a semantic for \DF, it does not provide any implementation and does question performance aspects of \DF.
We will provide performance mesurement for these constructions, and find out how they behave in practice (section \ref{sec:benchmarks}).

We will implement a librairy for \CF\ futures on \DF (refered to as Fut on Flow, see section \ref{sec:fut-on-flow}), and mesure its performance. We will seek to enable backward compatibility between \DF\ and \CF, by ensuring that \DF\ is compatible with a librairy implementation of futures (section \ref{sec:typing-forward}).

Finally, our work comparing the different solution to provide futures construction in Encore is the first work done using our version of the Encore compiler, with the \DF\ features, and will allow us to have the point of vue of a user on \DF, and find bugs (see section \ref{sec:others}).

