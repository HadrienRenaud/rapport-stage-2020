---
title: Rapport de stage
author: Hadrien Renaud
date: july 2020
---


# Introduction -- 2-3 pages (currently 0)

- À évoquer:
  - Actor model
  - Futures
  - explicit vs implicit
  - dataflow model ?
  - a word about chains of futures
  - one paragraph on encore

- contribs:
    - Show that it is feasable to implement Fut on Flow
    - Show that a Flow backend for Futures is a good choice in term of performances 
    - ¿¿ Practice Encore with Flows ??

- organisation du document

_écrire l'intro à la fin, la rédiger pour que le lambda mec puisse comprendre les CONTRIBS_


# Previous work -- 8 pages (currently 7)

## Encore -- 2 pages

- The language
- active objects, causal ordering, green threads
- How futures are implemented in Encore (controlflow explicit futures, main operators ...) 
- Run on pony, compiler structure, and properties
- Exemples 

## Introduction to DeF -- 2 pages

- Philosophy
- Overview of operators
- Basic example
- Impact on a chain of futures

## Semantic of DeF -- 2 pages

Here I'll take the semantic of Godot for Def and :

 - Merge the main semantic with the specialized for Godot
 - Rewrite explanations with the new merged semantic

## Introduction to forward -- 1.5 pages

- Motivation
- A little bit of semantics
- How it changes the chains of futures
- How it has been adapted to flows

# Motivation -- 2-3 pages (currently 3)

- Répondre aux questions:
    - facilité d'écriture
    - efficacité
    - faisabilité
- Experiment with our fork of the Encore compiler, and find bugs

- Example

# Fut-on-flow -- 3 pages (currently 2)

- Bref rappel de la motivation
- Introduce the box operetor (with semantics), obviously citing Godot
    - Definition of box
    - Semantic and types
    - Critique de Godot sur le type de forward (cf Typing forward)
- Deducing how fut on flow would work
- Implementation details

# Benchmarks -- 4 pages (currently 2)

- Rappel bref de la motivation
- Implementation:
  - Examples
  - A word on reproductibility and representativeness of those benchmarks
- Results:
  - Direct constatation
  - Conclusion on the performance of fut, flows and fut-on-flows

# Typing forward -- 3 pages (currently 2)

- Introduction
- _Paste here what i wrote for the article_
  _Don't forget to use "we" here_

# Other work -- 3 pages (currently .5)

## Debugging

- GC:
  - no documentation
  - link to github issue

> _btw, shouldn't we make a MR to update Encore docs on GC ?_

- Performance related issues
- A few mistakes in typing

## Examples and article writing

- blabla on passing time writing Encore code
- Ref of the article
- A note on proactive ?

# Conclusion -- 1 page (currently 0)



# Annexes

- A list of the repos I worked on (don't forget to link hals soft)

- Examples pour les benchmarks

